# Terraform and Ansible (Together Forever!)

The purpose of this project is as a demonstration of Ansible running inside GitLab CI alongside Terraform. As a project it will build AWS resources:

* VPC
* Subnet
* Security Groups
* EC2 Instance
* Elastic IP

Then run an Ansible playbook to manage configuration of the instance.

## Pre-requisites

* SSH (you surely have this)
* Key Pair for AWS

## Setup

1. You will need to set up an somewhere to store Terraform state.
2. You will need to pull the submodule to be able to deploy nginx. If this is not required then you should have everything: `git submodule --init --remote`

## Terraform

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ami | AMI of image | string | `"ami-0c3f128b7298d29b9"` | no |
| instance\_type | EC2 instance type | string | `"t2.small"` | no |
| key\_location | Location of the private key on local machine | string | `"~/.ssh/id_rsa"` | no |
| key\_name | AWS Key Pair Name | string | `"private-key"` | no |
| linux\_package\_manager | Linux package manager | string | `"apt-get"` | no |
| name | EC2 Instance name | map | `<map>` | no |
| project\_tags | Common project tags | map | `<map>` | no |
| region | AWS region for deployment | string | `"eu-west-2"` | no |
| remote\_ssh\_user | Remote user for SSH access | string | `"ubuntu"` | no |
| s3\_backend\_key | S3 key for storing state | string | `"demo/terratest/terraform.tfstate"` | no |
| s3\_bucket\_name | S3 bucket for storing state | string | `"bucket"` | no |

## Outputs

| Name | Description |
|------|-------------|
| ip\_address | External IP address of Elastic IP (Instance) |
| name | Name of instance |

## Maintainer

* Will Hall (willh@helecloud.com)
