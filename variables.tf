variable "region" {
  description = "AWS region for deployment"
  type        = string
  default     = "eu-west-2"
}

variable "ami" {
  description = "AMI of image"
  default     = "ami-0c3f128b7298d29b9" # eu-west-2 Ubuntu-18.04"
}

variable "instance_type" {
  description = "EC2 instance type"
  default     = "t2.small"
}

variable "name" {
  description = "EC2 Instance name"
  type        = map
  default = {
    Name = "Instance"
  }
}

variable "project_tags" {
  description = "Common project tags"
  type        = map
  default = {
    Owner      = "Will Hall"
    Purpose    = "Testing"
    CostCenter = "0001"
  }
}

variable "key_name" {
  type        = string
  description = "AWS Key Pair Name"
  default     = "willh-london"
}

variable "key_location" {
  type        = string
  description = "Location of the private key on local machine"
  default     = "~/.ssh/id_rsa"
}

variable "remote_ssh_user" {
  type        = string
  description = "Remote user for SSH access"
  default     = "ubuntu"
}

variable "linux_package_manager" {
  type        = string
  description = "Linux package manager"
  default     = "apt-get"
}
